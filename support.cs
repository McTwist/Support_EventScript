// Support file for various add-ons

// Set standard preference values
if ($Pref::EventScript::Labels::Generate $= "")
{
	$Pref::EventScript::Labels::Generate = 1;
}

// Default events
EventScript_modLabel("setEventEnabled", 0, 1);
EventScript_modLabel("toggleEventEnabled", 0, 1);

// Deprecated mods
// Event_doSub
EventScript_modLabel("doSub", 0, 1);
EventScript_modLabel("doSub", 1, 1);

// Load supported add-ons
%f = new FileObject();
for (%file = findFirstFile("Add-Ons/*/eventScriptLabels.txt");
	%file !$= "";
	%file = findNextFile("Add-Ons/*/eventScriptLabels.txt"))
{
	%f.openForRead(%file);
	while (!%f.isEOF())
	{
		%line = trim(%f.readLine());
		if (%line $= "")
			continue;
		if (getFieldCount(%line) < 3)
			continue;
		EventScript_modLabel(
			getField(%line, 0),
			getField(%line, 1) << 0,
			getField(%line, 2) << 0);
	}
	%f.close();
}
%f.delete();
